# Test Servier project

## ML test
Details can be found in ml_test_description.pdf
	
#### Docker
There is 2 dockerfiles in the repo, one is for devlopment purpose, the other one for production
  * servier -> minimal env to run script for a user or start the webapp
  * dockerfile.dev -> dockerfile with additionnal tools for development purpose (jupyter, pandas, matplotlib, pycharm, etc...)
	
The current docker is a multi stage build including tensorflow 2.3 and rdkit

To start the image, a command of the following type can be used:  
`docker run -it \`  
`-p *port_in_the_VM*:*port_on_your_localhost* \ # to open ports`  
`--rm \`  
`-v *dir_on_the_host_machine*:*dir_in_container* \ #to mount a volume`  
`-e DISPLAY=192.168.99.1:0 -v /tmp/.X11-unix:/tmp/.X11-unix \ #for export display (pycharm)`  
`*container_name*:*container_tag* *command_to_execute_in_the_docker*`
  * recommended port to open are:
    * 8888 for jupyter
    * 5000 for flask
    * 6006 for tensorboard
    * recommended volume is the root directory of the project `~/servier:/home/servier`
    
In the development docker:
  * The pycharm starter is at /bin/pycharm.sh
  * A jupyter notebook can be started with: `jupyter notebook --ip=0.0.0.0 --allow-root` (there is no user defined in the dev docker)

The note book will be accessible at -> localhost:*port_on_your_localhost*
For pycharm you need start an X server accepting configured to accepts incoming connections

#### Models
There are 2 models implemented in this repository:  

##### Model1
Model1 is a simple fully connected neural network. The default configuration has two layers of 128 hidden neurons that you can easily change by using the `--layers_size` argument of the command `servier train`

##### Model2
Model2 is a simplified version of the model that you can find in the paper:  
[Convolutional neural network based on SMILES representation of compounds for detecting chemical motif](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2523-5)

The simpler version comes down to the fact that having only the smiles of molecules, we can't build some features described in the paper. But the architecture of the model provided here is very similar to the one that can be found in the paper. Given the complexity of the model there is no entrypoint to modulate it yet. But it can be trained, evaluated and used to predict, as well as used in the web_app using the command provided
	
### setup.py
You can easily setup the app by typing `./setup.py install` in the working directory of the docker.
You will then have access to the command `servier` and the other subcommands:
  * `servier train`
  * `servier evaluate`
  * `servier predict`
  
 for more information about the commands and their options please use `servier *command_name* -help`
	
#### main.py
The main of the app is a very simple function without entrypoint. Launching the main will load data, initialize a model, train it, evaluate it, and predict the property for the given file.  

 /!\  To be noted, the main trains and predicts on the same dataset which is considered bad practice, it's a a usefool to evaluate a model but without entrypoint, for the time being, it is not a very useful function. A use case of the main is required to better tailor the entrypoint of the main.

#### Flask
In the working dir you can start the Flask API by using the web_app.py file:
  * `python web_app.py` if you want to use your own python interpreter
  * or simply `./web_app.py` directly from the working directory
The web app will be binded to you port 5000 and can be changed through the docker command
The Authentication on the app is:
  * login : servier
  * mdp : servier_molecule_properties
  
 /!\ The model loaded by web_app should exist at /data/ml_output/saved_models/model_web
  
The entry point of the app is at : servier/api/v0.1/predict/
You can either send a json containing {'smile':*smile*} or simply using the end point servier/api/v0.1/predict/*smile*

Example with Python request would be:  
`url = 'http://127.0.0.1:5000/servier/api/v0.1/predict/`  
`auth = ('servier', 'servier_molecule_properties')`  
`r = requests.post(url+*smile_as_a_string*, auth=auth)`  
or  
`url = 'http://127.0.0.1:5000/servier/api/v0.1/predict/`  
`auth = ('servier', 'servier_molecule_properties')`  
`smile_prep_json = {'smile' : *smile_as_a_string*}`  
`smile_json = json.dumps(smile_prep_json)`  
`headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}`  
`r = requests.post(url, data=smile_json, headers=headers, auth=auth)`  


## Aknowledgement
Once again I would like to mention and thank the team who wrote 'Convolutional neural network based on SMILES representation of compounds for detecting chemical motif' whose work has heavily inspired my architecture for model2:  
  * The link to their article : [Convolutional neural network based on SMILES representation of compounds for detecting chemical motif](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2523-5)  
  * And the code of their model : [Repo](http://www.dna.bio.keio.ac.jp/smiles/)