# Got an rdkit version working for python 3 thx to https://github.com/mcs07/docker-rdkit

FROM debian:buster-slim AS rdkit-build-env

RUN apt-get update \
 && apt-get install -yq --no-install-recommends \
    ca-certificates \
    build-essential \
    cmake \
    wget \
    libboost-dev \
    libboost-iostreams-dev \
    libboost-python-dev \
    libboost-regex-dev \
    libboost-serialization-dev \
    libboost-system-dev \
    libboost-thread-dev \
    libcairo2-dev \
    libeigen3-dev \
    python3-dev \
    python3-numpy \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

ARG RDKIT_VERSION=Release_2020_03_2
RUN wget --quiet https://github.com/rdkit/rdkit/archive/${RDKIT_VERSION}.tar.gz \
 && tar -xzf ${RDKIT_VERSION}.tar.gz \
 && mv rdkit-${RDKIT_VERSION} rdkit \
 && rm ${RDKIT_VERSION}.tar.gz

RUN mkdir /rdkit/build
WORKDIR /rdkit/build

RUN cmake -Wno-dev \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -D Boost_NO_BOOST_CMAKE=ON \
    -D PYTHON_EXECUTABLE=/usr/bin/python3 \
    -D RDK_BUILD_AVALON_SUPPORT=ON \
    -D RDK_BUILD_CAIRO_SUPPORT=ON \
    -D RDK_BUILD_CPP_TESTS=OFF \
    -D RDK_BUILD_INCHI_SUPPORT=ON \
    -D RDK_BUILD_FREESASA_SUPPORT=ON \
    -D RDK_INSTALL_INTREE=OFF \
    -D RDK_INSTALL_STATIC_LIBS=OFF \
    ..

RUN make -j $(nproc) \
 && make install

FROM debian:buster-slim AS rdkit-env

# Install runtime dependencies
RUN apt-get update \
 && apt-get install -yq --no-install-recommends \
    libboost-atomic1.67.0 \
    libboost-chrono1.67.0 \
    libboost-date-time1.67.0 \
    libboost-iostreams1.67.0 \
    libboost-python1.67.0 \
    libboost-regex1.67.0 \
    libboost-serialization1.67.0 \
    libboost-system1.67.0 \
    libboost-thread1.67.0 \
    libcairo2-dev \
    python3-dev \
    python3-numpy \
    python3-cairo \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# Copy rdkit installation from rdkit-build-env
COPY --from=rdkit-build-env /usr/lib/libRDKit* /usr/lib/
COPY --from=rdkit-build-env /usr/lib/cmake/rdkit/* /usr/lib/cmake/rdkit/
COPY --from=rdkit-build-env /usr/share/RDKit /usr/share/RDKit
COPY --from=rdkit-build-env /usr/include/rdkit /usr/include/rdkit
COPY --from=rdkit-build-env /usr/lib/python3/dist-packages/rdkit /usr/lib/python3/dist-packages/rdkit

FROM rdkit-env

RUN apt-get update \
 && apt-get install -y \
	curl \
	dos2unix \
	python3-pip \
	gcc git openssh-client less curl \
	libxtst-dev libxext-dev libxrender-dev libfreetype6-dev \
	libfontconfig1 libgtk2.0-0 libxslt1.1 libxxf86vm1 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN pip3 --no-cache-dir install --ignore-installed --upgrade ipython && \
	pip3 install --upgrade pip && \
	pip3 --no-cache-dir install \
		Cython \
		flask \
		flask-BasicAuth \
		flask-HTTPAuth \
		ipywidgets \
		ipykernel \
		jupyter \
		keras \
		matplotlib \
		tensorflow \
		pandas \
		scikit-learn \
		&& \
	python3 -m ipykernel.kernelspec
	
RUN mkdir /home/servier_app

ARG PYCHARM_VERSION=2019.3
ARG PYCHARM_BUILD=2019.3.2
ARG pycharm_source=https://download.jetbrains.com/python/pycharm-community-${PYCHARM_BUILD}.tar.gz
ARG pycharm_local_dir=.PyCharmCE${PYCHARM_VERSION}

RUN mkdir /opt/pycharm && \
	cd /opt/pycharm
	
RUN curl -fsSL $pycharm_source -o /opt/pycharm/installer.tgz \
  && tar --strip-components=1 -xzf /opt/pycharm/installer.tgz \
  && rm /opt/pycharm/installer.tgz

#RUN useradd -ms /bin/bash newuser

RUN mkdir /home/servier_app/.PyCharm \
  && ln -sf /home/servier_app.PyCharm /home/servier_app$pycharm_local_dir

#USER newuser
	
WORKDIR /home/servier_app