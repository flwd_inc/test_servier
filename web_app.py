#!/usr/bin/env python3

from flask import Flask, jsonify
from flask import abort
from flask import make_response
from flask import request
import codecs, json
from flask_httpauth import HTTPBasicAuth


import numpy as np
import servier.utils.utils_servier as us


app = Flask(__name__)
auth = HTTPBasicAuth()


def predict_P1_prop_from_smile(smile):
	fingerprint = us.smile_to_fingerprint(smile)
	model = us.load_model('data/ml_output/saved_models/model_web')
	return us.cont_pred_to_class(model.predict(fingerprint))


@auth.get_password
def get_password(username):
    if username == 'servier':
        return 'servier_molecule_properties'
    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)


@app.route('/servier/api/v0.1/predict/<smile>', methods=['POST'])
@auth.login_required
def predict_property_P1_url(smile):

    y_pred = predict_P1_prop_from_smile(smile)

    return jsonify({smile: {'P1':str(y_pred[0][0])}})

@app.route('/servier/api/v0.1/predict/', methods=['POST'])
@auth.login_required
def predict_property_P1_json():
    if not request.json or not "smile" in request.json:
        abort(400)

    smile = request.json['smile']
    y_pred = predict_P1_prop_from_smile(smile)

    return jsonify({smile: {'P1': str(y_pred[0][0])}})


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == '__main__':
    app.run(host='0.0.0.0')