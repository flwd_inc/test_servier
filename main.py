#!/usr/bin/env python3
import os

import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.metrics import roc_auc_score

import servier.utils.utils_servier as us
import servier.commands.train as tr
import servier.commands.predict as pr


def cross_validate(model, X, Y, split_size=5):
    results = []
    kf = KFold(n_splits=split_size)
    model.compile(optimizer='adam',
                  loss=tf.keras.losses.BinaryCrossentropy())
    for train_index, test_index in kf.split(X):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = Y[train_index], Y[test_index]
        model.fit(X_train,
                  y_train,
                  epochs=10)
        y_pred = us.cont_pred_to_class(model.predict(X_test))
        score = roc_auc_score(y_pred, y_test)
        print("%s: %.2f%%" % ('ROC_AUC', score * 100))
        results.append(score)
    return np.mean(results)


if __name__=='__main__':

    input_file = 'data/ml_input/dataset_single.csv'
    features_col = 'smiles'
    id_col = 'mol_id'
    target_col = 'P1'
    model_layers_size = [64, 64]
    split_size = 5
    output_file_path = 'data/ml_output/'
    output_file_name = 'pred.csv'

    ds = us.load_data(input_file)
    fingerprints = us.smiles_to_fingerprints((ds[features_col]))

    model = tr.initialize_model1(model_layers_size)

    score = cross_validate(model, fingerprints, ds[target_col].to_numpy(), split_size=split_size )
    print("%s: %.2f%%" % ('ROC_AUC_avg', score*100))

    ds['%s_pred'%target_col] = pr.predict(model, fingerprints)

    us.save_data(ds[[id_col, '%s_pred'%target_col]], output_file_path, output_file_name)