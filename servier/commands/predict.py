import argparse

import servier.utils.utils_servier as us


def arg_parser(args):
    parser = argparse.ArgumentParser('Parsing training arguments')
    parser.add_argument('--input_model_path', type=str, default='data/ml_output/saved_models/model', \
                        help='The path to the model to use for prediction')
    parser.add_argument('--input_data_path', type=str, default='data/ml_input/dataset_single.csv', \
                        help='The path to the data file to use as an input')
    parser.add_argument('--smiles_col', type=str, default='smiles', help='The name of the column of where smiles are \
                            stored')
    parser.add_argument('--target_col', type=str, default='P1', help='The name of the column to predict')
    parser.add_argument('--id_col', type=str, default='mol_id', help='The name of the column to use as id')
    parser.add_argument('--model_name', type=str, default='model1', help='Specify the model to train')
    parser.add_argument('--output_data_path', type=str, default='data/ml_output/', \
                        help='The path to where to output the predictions')
    parser.add_argument('--output_data_filename', type=str, default='pred.csv', \
                        help='The name of the file to save the output')
    parser.add_argument('--save_pred', type=bool, default=True, \
                        help='A variable to indicate if the predicted data should be saved or not')
    return parser.parse_args(args)


def predict(model, X_pred):
    print('Calculating predictions')
    return us.cont_pred_to_class(model.predict(X_pred))


class Command():
    def run(self, args):
        parsed_args = arg_parser(args)
        model = us.load_model(parsed_args.input_model_path)

        ds = us.load_data(parsed_args.input_data_path)

        if parsed_args.model_name == 'model1':
            fingerprints = us.smiles_to_fingerprints((ds[parsed_args.smiles_col]))
            ds['%s_pred' % parsed_args.target_col] = predict(model, fingerprints)
        if parsed_args.model_name == 'model2':
            smiles = ds[parsed_args.smiles_col].to_numpy()
            symbol_list, longest_smile_size = us.extract_longest_and_symbol_list_from_array(smiles)
            encoded_smiles_array = us.smiles_to_one_hot(smiles, longest_smile_size, symbol_list)
            ds['%s_pred' % parsed_args.target_col] = predict(model, encoded_smiles_array)
        if parsed_args.model_name == 'model3':
            pass

        if parsed_args.save_pred:
            us.save_data(ds[[parsed_args.id_col,'%s_pred' % parsed_args.target_col]], parsed_args.output_data_path, \
                         parsed_args.output_data_filename)