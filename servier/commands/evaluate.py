import argparse

from sklearn.metrics import roc_auc_score
import servier.utils.utils_servier as us


def arg_parser(args):
    parser = argparse.ArgumentParser('Parsing training arguments')
    parser.add_argument('--input_model_path', type=str, default='data/ml_output/saved_models/model', \
                        help='The path to the model to use for prediction')
    parser.add_argument('--input_data_path', type=str, default='data/ml_input/dataset_single.csv', \
                        help='The path to the data file to use as an input')
    parser.add_argument('--smiles_col', type=str, default='smiles', help='The name of the column of where smiles are \
                            stored')
    parser.add_argument('--target_col', type=str, default='P1', help='The name of the column to predict')
    parser.add_argument('--model_name', type=str, default='model1', help='Specify the model to train')
    return parser.parse_args(args)

def evaluate_model(model, X_test, y_test):
    y_pred = us.cont_pred_to_class(model.predict(X_test))
    score = roc_auc_score(y_pred, y_test)
    print("%s: %.2f%%" % ('ROC_AUC', score * 100))


class Command():
    def run(self, args):
        parsed_args = arg_parser(args)
        model = us.load_model(parsed_args.input_model_path)

        ds = us.load_data(parsed_args.input_data_path)
        y_test = ds[parsed_args.target_col].to_numpy()

        if parsed_args.model_name=='model1':
            fingerprints = us.smiles_to_fingerprints(ds[parsed_args.smiles_col])
            evaluate_model(model, fingerprints, y_test)
        if parsed_args.model_name=='model2':
            smiles = ds[parsed_args.smiles_col].to_numpy()
            symbol_list, longest_smile_size = us.extract_longest_and_symbol_list_from_array(smiles)
            encoded_smiles_array = us.smiles_to_one_hot(smiles, longest_smile_size, symbol_list)
            evaluate_model(model, encoded_smiles_array, y_test)
        if parsed_args.model_name=='model3':
            pass