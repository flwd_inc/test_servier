import sys
import argparse

import tensorflow as tf
from keras import layers

import servier.utils.utils_servier as us


def arg_parser(args):
    parser = argparse.ArgumentParser('Parsing training arguments')
    parser.add_argument('--input_data_path', type=str, default='data/ml_input/dataset_single.csv', \
                        help='The path to the data file to use as an input')
    parser.add_argument('--smiles_col', type=str, default='smiles', help='The name of the column of where smiles are \
                        stored')
    parser.add_argument('--target_col', type=str, default='P1', help='The name of the column to predict')
    parser.add_argument('--model_name', type=str, default='model1', help='Specify the model to train [model1, model2]')
    parser.add_argument('--layers_size', metavar='N', type=int, nargs='+', default=[128, 128], \
                        help='The sizes of the hidden layers for the model')
    parser.add_argument('--output_model_path', type=str, default='data/ml_output/saved_models/', \
                        help='The dir where the trained model will be saved')
    parser.add_argument('--output_model_file', type=str, default='model', \
                        help='The name of the file where the model will be saved')
    parser.add_argument('--save_model', type=bool, default=True, \
                        help='A variable to indicate if the model should be saved or not')
    return parser.parse_args(args)


def initialize_model1(layers_size):
    print('Initializing and building the model')
    layers_list = []

    for size in layers_size:
        layers_list.append(layers.Dense(size, activation='relu'))
    layers_list.append(layers.Dropout(.1))
    layers_list.append(layers.Dense(1))

    model = tf.keras.Sequential(layers_list)

    return model


def initialize_model2():
    print('Initializing and building the model')
    model = tf.keras.Sequential([
        layers.Conv2D(128, 11, strides=1, padding='same', data_format='channels_last'),
        layers.BatchNormalization(),
        layers.LeakyReLU(),
        layers.AveragePooling2D(pool_size=(5, 1), strides=None, padding='valid', data_format='channels_last'),

        layers.Conv2D(64, 5, strides=1, padding='same', data_format='channels_last'),
        layers.BatchNormalization(),
        layers.LeakyReLU(),
        layers.AveragePooling2D(pool_size=(5, 1), strides=1, padding='same', data_format='channels_last'),

        layers.MaxPool2D(pool_size=(2, 1), strides=1, padding='same'),
        layers.Flatten(),

        layers.Dense(96, activation='relu'),
        layers.BatchNormalization(),

        layers.Dense(1, activation='relu'),
    ])

    return model


def initialize_model3():
    pass


def train_model(model, X_train, y_train, optimizer='adam', loss=tf.keras.losses.BinaryCrossentropy(), \
                metrics=None, epochs=10):
    print('Training the model')
    model.compile(optimizer=optimizer,
                  loss=loss,
                  metrics=metrics)
    model.fit(X_train,
              y_train,
              epochs=epochs)
    return model


class Command():

    def run(self, args):
        parsed_args = arg_parser(args)
        ds = us.load_data(parsed_args.input_data_path)

        if parsed_args.model_name=='model1':
            fingerprints = us.smiles_to_fingerprints((ds[parsed_args.smiles_col]))
            model = initialize_model1(parsed_args.layers_size)
            model = train_model(model, fingerprints, ds[parsed_args.target_col])
        if parsed_args.model_name=='model2':
            smiles = ds[parsed_args.smiles_col].to_numpy()
            symbol_list, longest_smile_size = us.extract_longest_and_symbol_list_from_array(smiles)
            encoded_smiles_array = us.smiles_to_one_hot(smiles, longest_smile_size, symbol_list)
            model = initialize_model2()
            model = train_model(model, encoded_smiles_array, ds[parsed_args.target_col])
        if parsed_args.model_name=='model3':
            model = initialize_model3()

        if parsed_args.save_model:
            us.save_model(model, parsed_args.output_model_path, parsed_args.output_model_file)