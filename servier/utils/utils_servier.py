import os

from pandas import read_csv
import numpy as np
import tensorflow as tf

import servier.utils.feature_extractor as fe

def create_path_recursive(path):
    i = 2
    while not os.path.isdir(path):
        print('Checking if the path %s exists' % path)
        if not os.path.isdir("/".join(path.split('/')[0:i])):
            os.mkdir("/".join(path.split('/')[0:i]))
        i += 1


def save_data(ds, save_dir, file_name):
    create_path_recursive(save_dir)
    print('Saving dataset')
    ds.to_csv(save_dir+file_name, index=False)
    print('Dataset saved at %s' % (save_dir+file_name))


def load_data(file_path):
    print('Loading %s' % file_path)
    ds = read_csv(file_path)
    return ds


def save_model(model, save_dir, model_name):
    create_path_recursive(save_dir)
    print('Saving model')
    model.save(save_dir+model_name)
    print('Model saved at %s' % (save_dir+model_name))


def load_model(load_dir):
    print('Loading model fom %s' % load_dir)
    model = tf.keras.models.load_model(load_dir)
    print('Model loaded')
    return model


def smiles_to_fingerprints(smiles):
    to_fingerprint = lambda x : fe.fingerprint_features(x).ToBitString()
    to_int_array = lambda x: np.array([int(i) for i in to_fingerprint(x)])
    print('Converting smiles to figerprints array...')
    return np.stack([to_int_array(smile) for smile in smiles])


def smile_to_fingerprint(smile):
    to_fingerprint = lambda x: fe.fingerprint_features(x).ToBitString()
    to_int_array = lambda x: np.array([int(i) for i in to_fingerprint(x)])
    print('Converting smiles to figerprints array...')
    return to_int_array(smile).reshape(1, 2048)


def cont_pred_to_class(array):
    pred_array = 0.5<array
    return pred_array.astype('int32')


def extract_longest_and_symbol_list_from_array(smiles):
    symbol_set = set()
    for smile in smiles:
        symbol_set = symbol_set.union(set(smile))
    symbol_list = np.array(list(symbol_set))
    longest_smile_size = max([len(smile) for smile in smiles])
    return symbol_list, longest_smile_size


def smiles_to_one_hot(smiles_array, longest_smile_size, symbol_list):
    encoded_smiles = []
    for smile in smiles_array:
        one_hot = []
        for char in smile:
            one_hot.append((symbol_list == char).astype(np.int32))
        for i in range(longest_smile_size - len(smile)):
            one_hot.append([0] * len(symbol_list))
        encoded_smiles.append(np.stack(one_hot))
    return np.stack(encoded_smiles)[..., np.newaxis].astype(np.float64)
